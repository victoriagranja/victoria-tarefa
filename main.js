const url = 'https://in-api-treinamento.herokuapp.com/posts'

const myHeaders = {
    "content-Type": "application/json"
}

let botaoEnviar = document.querySelector("#submit");
botaoEnviar.addEventListener('click', (e) => {
    let nome = document.querySelector("#nome").value;
    let mensagem = document.querySelector("#mensagem").value;

    const novoPost = {
        "post":{
            "name": nome,
            "message": mensagem
        }
    }

    const fetchConfig = {
        method: "POST",
        headers: myHeaders,
        body: JSON.stringify(novoPost)
    }

    fetch(url, fetchConfig)
    .then(console.log)
    .catch(() => alert("Não foi"))

    e.preventDefault();

})

const pai = document.querySelector("#list");

fetch(url)
.then(resposta => resposta.json())
.then(respostaJson => {
    respostaJson.forEach((itemArray) => {
        let {name, message} = itemArray;

        let div = document.createElement("div");
        let but = document.createElement("button");

        div.innerText = `nome: ${name} mensagem: ${message}`;
        but.innerText = "Remover"

        but.classList.add("excluir")
        
        pai.appendChild(div);
        div.appendChild(but);

    })
})
.catch(() => alert("Não foi"))

// function deleteData(item, url) {
//     return fetch(url + '/' + item, {
//       method: 'delete'
//     })
//     .then(response => response.json());
//   }

pai.addEventListener('click', e => {
    e.target.parentNode.remove();
    fetch(url+'/'+id)
    e.preventDefault();
})

console.log(url+'/'+'5')

fetch(url+'/'+'5', {
    method: "DELETE",
}).then(resp => resp.json()).catch(console.log())